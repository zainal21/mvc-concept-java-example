
package restoran.dao;

import restoran.model.Menu;
import java.util.List;
public interface implementMenu {
    public void insert (Menu b);
    public void update (Menu b);
    public void delete (int id);
    public List<Menu> getALL();
}
