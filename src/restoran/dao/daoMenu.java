
package restoran.dao;
import restoran.koneksi.koneksi;
import restoran.model.Menu;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;

    public class daoMenu implements implementMenu{
        Connection connection;
            final String insert = "INSERT INTO menu (nama,harga,stok,kategori,favorit) VALUES (?, ?, ?, ?, ?)";
            final String update = "UPDATE menu set nama=?, harga=?, stok=?, kategori=?, favorit=? where id=? ";
            final String delete = "DELETE FROM menu where id=? ";
            final String select = "SELECT * FROM menu";

    
public daoMenu(){
    connection = koneksi.connection();
    
}
    public void insert(Menu b) {
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement(insert);
            statement.setString(1, b.getNama());
            statement.setFloat (2, b.getHarga());
            statement.setInt(3, b.getStok());
            statement.setString(4,b.getKategori());
            statement.setBoolean(5, b.getFavorit());
            statement.executeUpdate();
            ResultSet rs = statement.getGeneratedKeys();
            while (rs.next()) {
                b.setId(rs.getInt(1));
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            try {
                statement.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
    }

public void update (Menu b){
        PreparedStatement statement= null;
        try{
            statement=connection.prepareStatement(update);
            statement.setString(1, b.getNama());
            statement.setFloat(2, b.getHarga());
            statement.setInt(3, b.getStok());
            statement.setString(4, b.getKategori());
            statement.setBoolean(5, b.getFavorit());
            statement.setInt(6, b.getId());
            statement.executeUpdate();
            
        }   catch (SQLException ex) {
            ex.printStackTrace();
        }
    }
public void delete (int id){
        PreparedStatement statement= null;
        try{
            statement=connection.prepareStatement(delete);
            statement.setInt(1, id);
            statement.executeUpdate();
            
        }   catch (SQLException ex) {
            ex.printStackTrace();
        }finally{    
            try{
                statement.close();
            }catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
    }

public List<Menu> getALL() {
        List<Menu> lb = null;
        try {
            lb = new ArrayList<Menu>();
            Statement st = connection.createStatement();
            ResultSet rs = st.executeQuery(select);
            while (rs.next()) {
                Menu b = new Menu();
                b.setId(rs.getInt("id"));
                b.setNama(rs.getString("nama"));
                b.setHarga(rs.getFloat("harga"));
                b.setStok(rs.getInt("stok"));
                b.setKategori(rs.getString("kategori"));
                b.setFavorit(rs.getBoolean("favorit"));
                lb.add(b);
            }
        } catch (SQLException ex) {
            Logger.getLogger(daoMenu.class.getName()).log(Level.SEVERE, null, ex);
        }

        return lb;
    }
}
