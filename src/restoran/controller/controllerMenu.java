
package restoran.controller;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import restoran.dao.daoMenu;
import restoran.model.Menu;
import restoran.model.tableModelRestoran;
import restoran.view.FrameRestoran;
import java.util.List;
import javax.swing.JOptionPane;
import restoran.dao.implementMenu;
import restoran.koneksi.koneksi;

/**
 *
 * @author shdkhar
 */
public class controllerMenu {
    FrameRestoran frame;
    implementMenu implMenu;
    List<Menu>lb;
    public controllerMenu(FrameRestoran frame) {
        this.frame = frame;
        implMenu = new daoMenu();
        lb = implMenu.getALL();
    }
    public void reset(){
        frame.getTxtID().setText("");
        frame.getTxtNama().setText("");
        frame.getTxtHarga().setText("");
        frame.getTxtStok().setText("");
        frame.getCheckboxFavorit().setSelected(false);
        frame.getComboKategori().setSelectedItem(null);
        
    }
    public void isiTable(){
        lb = implMenu.getALL();
        tableModelRestoran tmb = new tableModelRestoran(lb);
        frame.getTabelData().setModel(tmb);
    }
    public void isiField(int row){
         frame.getTxtID().setText(lb.get(row).getId().toString());
         frame.getTxtStok().setText(lb.get(row).getStok().toString());
         frame.getTxtNama().setText(lb.get(row).getNama());
         frame.getTxtHarga().setText(lb.get(row).getHarga().toString());
         frame.getComboKategori().setSelectedItem(lb.get(row).getKategori());
         frame.getCheckboxFavorit().setSelected(lb.get(row).getFavorit());
    }
    public void insert(){
        Menu b= new Menu();
        b.setStok(Integer.parseInt(frame.getTxtStok().getText()));
        b.setNama(frame.getTxtNama().getText());
        b.setHarga(Float.parseFloat(frame.getTxtHarga().getText()));
        b.setKategori((String) frame.getComboKategori().getSelectedItem());
        b.setFavorit(frame.getCheckboxFavorit().isSelected());
//        b.setId(Integer.parseInt(frame.getTxtID().getText()));
        implMenu.insert(b);        
    }
    public void update(){
        Menu b= new Menu();
        b.setStok(Integer.parseInt(frame.getTxtStok().getText()));
        b.setNama(frame.getTxtNama().getText());
        b.setHarga(Float.parseFloat(frame.getTxtHarga().getText()));
        b.setKategori((String) frame.getComboKategori().getSelectedItem());
        b.setFavorit(frame.getCheckboxFavorit().isSelected());
        b.setId(Integer.parseInt(frame.getTxtID().getText()));
        implMenu.update(b);
    }
    public void delete(){
        if(!frame.getTxtID().getText().trim().isEmpty()){
            int id=Integer.parseInt(frame.getTxtID().getText());
            implMenu.delete(id);
        }else{
            JOptionPane.showMessageDialog(frame,"pilih data yang akan dihapus" );
        }
    }
    
    
}






