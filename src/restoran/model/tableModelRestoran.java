
package restoran.model;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;
import javax.swing.table.AbstractTableModel;

public class tableModelRestoran extends AbstractTableModel{
    List <Menu> lb;
    public tableModelRestoran(List<Menu> lb){
        this.lb = lb;
    }
    @Override
    public int getColumnCount(){
        return 4;
    }
    public int getRowCount(){
        return lb.size();
    }
    @Override
    public String getColumnName(int column){
        switch(column){
            case 0:
            return "Nama Produk";
            case 1:
            return "Harga";
            case 2:
            return "Stok";
            case 3:
            return "Kategori";
            default:
                return null;
        }
    }
    @Override
   public Object getValueAt(int row, int column){
       switch(column){
           case 0:
           return lb.get(row).getNama();
            case 1:
           return getFormattedPrice(lb.get(row).getHarga());
            case 2:
           return lb.get(row).getStok();
            case 3:
           return lb.get(row).getKategori();
            default:
                return null;
       }
   } 
   
private String getFormattedPrice(Float value){
        DecimalFormat formatter = (DecimalFormat) NumberFormat.getInstance(Locale.getDefault());
        DecimalFormatSymbols symbols = formatter.getDecimalFormatSymbols();

        symbols.setGroupingSeparator('.');
        symbols.setMonetaryDecimalSeparator(',');
        formatter.setDecimalFormatSymbols(symbols);
        return "Rp "+formatter.format(value);        
        
//        return NumberFormat.getIntegerInstance(getLocale()).format(value);
}
}
