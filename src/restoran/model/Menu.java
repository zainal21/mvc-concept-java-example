package restoran.model;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.util.Locale;


public class Menu {
    private Integer id;
    private String nama;
    private Float harga;
    private Integer stok;
    private String kategori;
    private Boolean favorit;
   
    public void setHarga(Float harga) {
        this.harga = harga;
    }

    public void setStok(Integer stok) {
        this.stok = stok;
    }

    public void setKategori(String kategori) {
        this.kategori = kategori;
    }

    public void setFavorit(Boolean favorit) {
        this.favorit = favorit;
    }
    
    public void setNama(String nama){
        this.nama = nama;
    }
    
    public void setId(Integer id){
        this.id = id;
    }
    
    public Integer getId(){
        return id;
    }
    public String getNama(){
        return nama;
    }
    
    public Float getHarga(){
        return harga;
    }
    public String getKategori(){
        return kategori;
    }
    public Integer getStok(){
        return stok ;
    }
    public Boolean getFavorit(){
        return favorit;
    }
}
